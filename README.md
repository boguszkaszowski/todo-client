# todo-client

## Project setup

### Docker

  If you want to run an app using docker, you have tu run
  `docker-compose up` in the `path_to_project/todo-client` directory.
  
### Old fashioned way

  In the `path_to_project/todo-client` directory run:
  1. `yarn install`
  2. `yarn start`
  
  
### Api endpoints:


#### Fetch Todos
**URL:** `api-todo-app.s1.themasters.io/todos`
**METHOD:** GET  
**PARAMS:** NO PARAMS

#### Create Todo
**URL:** `api-todo-app.s1.themasters.io/todos`
**METHOD:** POST  
**PARAMS:**

```
{
  "description": "bla bla bla",
 }
```

#### Update Todo
**URL:** `api-todo-app.s1.themasters.io/todos/:id`
**METHOD:** PUT  
**PARAMS:**

```
{
  "done": true,
  "description": "trololo"
 }
```


#### Delete Todo
**URL:** `api-todo-app.s1.themasters.io/todos/:id`
**METHOD:** DELETE  
**PARAMS:** NO PARAMS
