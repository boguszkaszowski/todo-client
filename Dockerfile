FROM node:9.10.0-alpine

RUN mkdir -p /srv/app/todo-client
WORKDIR /srv/app/todo-client
COPY package.json /srv/app/todo-client

RUN yarn
ADD . /srv/app/todo-client
CMD [ "yarn", "build"]