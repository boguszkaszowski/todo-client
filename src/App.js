import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { Home } from './pages/Home';
import { NewTask } from './pages/NewTask';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Route path="/" exact component={Home} />
          <Route path="/new" exact component={NewTask} />
        </div>
      </Router>
    );
  }
}

export default App;
