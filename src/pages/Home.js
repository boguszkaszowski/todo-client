import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getTasks as getTasksService } from '../services/Task';

const Home = () => {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    getTasks();
  }, []);

  const getTasks = async () => {
    const result = await getTasksService();
    setTasks(result.data);
  };

  return (
    <div className="App">
      <Link to="/new">Add new</Link>

      <ul>
        {tasks.map((task, index) => (
          <li key={index}>{task.description}</li>
        ))}
      </ul>
    </div>
  );
};

export { Home };
