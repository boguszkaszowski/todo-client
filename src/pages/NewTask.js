import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TextField } from '../components/Inputs';
import { Button } from '../components/Button';
import { ErrorMessage } from '../components/ErrorMessage';
import { saveTask } from '../services/Task';

const NewTask = ({ history }) => {
  const [description, setDescription] = useState('');
  const [error, setError] = useState(null);

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      await saveTask({ description });
      history.push('/');
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <React.Fragment>
      {error && <ErrorMessage text={error} />}

      <form onSubmit={handleSubmit}>
        <TextField placeholder="Task description" value={description} onChange={setDescription} />
        <Button type="submit" text="Save" />
      </form>
    </React.Fragment>
  );
};

NewTask.propTypes = {
  history: PropTypes.object.isRequired,
};

export { NewTask };
