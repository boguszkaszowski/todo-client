import React from 'react';
import PropTypes from 'prop-types';

const TextField = ({ placeholder, value, onChange }) => <input type="text" value={value} onChange={e => onChange(e.target.value)} placeholder={placeholder} />;

TextField.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

TextField.defaultProps = {
  placeholder: '',
};

export { TextField };
