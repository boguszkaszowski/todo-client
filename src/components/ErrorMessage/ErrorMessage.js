import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const ErrorMessage = ({ text }) => (
  <div className="error">
    <p className="error__test">{text}</p>
  </div>
);

ErrorMessage.propTypes = {
  text: PropTypes.string.isRequired,
};

export { ErrorMessage };
