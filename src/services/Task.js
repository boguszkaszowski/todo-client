import axios from 'axios';

const baseUrl = 'http://api-todo-app.s1.themasters.io';

export const saveTask = ({ description }) => {
  return axios
    .post(`${baseUrl}/todos`, {
      description,
    })
    .catch(error => {
      throw new Error(error.message);
    });
};

export const getTasks = () => {
  return axios.get(`${baseUrl}/todos`);
};
